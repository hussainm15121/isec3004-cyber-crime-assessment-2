//Vulnerable Code

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <stdio.h>

struct UserAcc {
  char UserName[30];
  int UserAcc;        //1 -> password is set; 0 -> password is not set
};

struct UserAcc *UserAcc;
char *Option;

int main(int argc, char **argv)
{
  char line[128];

  while(1) 
  {
     printf("[ UserOne = %p, Option = %p ]\n", UserAcc, Option);

    if(fgets(line, sizeof(line), stdin) == NULL)
    {
        break;
    }
    
    if(strncmp(line, "Username: ", 10) == 0) 
    {
      UserAcc = malloc(sizeof(UserAcc));
      memset(UserAcc, 0, sizeof(UserAcc));
      if(strlen(line + 10) < 29) 
      {
        strcpy(UserAcc->UserName, line + 10);
      }
      printf("Username is: %s\n", UserAcc->UserName);
    }
    if(strncmp(line, "reset", 5) == 0) 
    {
      free(UserAcc);
    }
    if(strncmp(line, "Option ", 7) == 0) 
    {
      Option = strdup(line + 7);
    }
    if(strncmp(line, "Authenticate", 12) == 0) 
    {
      if(UserAcc->UserAcc) 
      {
        printf("you have been authenticated!\n");
      } 
      else 
      {
        printf("you need to set a password\n");
      }
    }
    if(strncmp(line, "Exit",4) == 0)
    {
        exit(0);
    }
  }
}

